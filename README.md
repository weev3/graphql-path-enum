# graphql-path-enum

This is a tool that lists the different ways of reaching a given type in a GraphQL schema. It takes the introspection query result as input. Given that most graphs have loops and have an infinite amount of paths, the tool doesn't list them _all_, but it does a relatively exhaustive listing nonetheless. You can read the [blog post](https://blog.deesee.xyz/graphql/security/2020/04/13/graphql-permission-testing.html) I wrote about the tool.

## Install

## Precompiled binaries

Visit the [pipelines](https://gitlab.com/dee-see/graphql-path-enum/pipelines) page and grab either the Linux or Windows binary from the latest `master` build ([macOS version coming eventually](https://gitlab.com/dee-see/graphql-path-enum/-/issues/3), comment in the issue to let me know there's interest).

## From sources

If Rust isn't already installed, read [these instructions](https://www.rust-lang.org/tools/install) to do so. Once that's done, you can optionally install the formatting and linting tools with `rustup component add rustfmt` and `rustup component add clippy` respectively.

Then simply clone the repo and `cargo build`.

## Demo

```shell
$ graphql-path-enum -i ./test_data/h1_introspection.json -t Skill
Found 27 ways to reach the "Skill" node from the "Query" node:
- Query (assignable_teams) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (checklist_check) -> ChecklistCheck (checklist) -> Checklist (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (checklist_check_response) -> ChecklistCheckResponse (checklist_check) -> ChecklistCheck (checklist) -> Checklist (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (checklist_checks) -> ChecklistCheck (checklist) -> Checklist (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (clusters) -> Cluster (weaknesses) -> Weakness (critical_reports) -> TeamMemberGroupConnection (edges) -> TeamMemberGroupEdge (node) -> TeamMemberGroup (team_members) -> TeamMember (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (embedded_submission_form) -> EmbeddedSubmissionForm (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (external_program) -> ExternalProgram (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (external_programs) -> ExternalProgram (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (job_listing) -> JobListing (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (job_listings) -> JobListing (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (me) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (pentest) -> Pentest (lead_pentester) -> Pentester (user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (pentests) -> Pentest (lead_pentester) -> Pentester (user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (query) -> Query (assignable_teams) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (query) -> Query (skills) -> Skill
- Query (report) -> Report (bounties) -> Bounty (invitations) -> InvitationsClaimBounty (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (report_retest_user) -> ReportRetestUser (invitation) -> InvitationsRetest (report) -> Report (bounties) -> Bounty (invitations) -> InvitationsClaimBounty (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (reports) -> TeamMemberGroupConnection (edges) -> TeamMemberGroupEdge (node) -> TeamMemberGroup (team_members) -> TeamMember (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (skills) -> Skill
- Query (sla_statuses) -> SlaStatus (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (teams) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (triage_inbox_items) -> TriageInboxItem (report) -> Report (bounties) -> Bounty (invitations) -> InvitationsClaimBounty (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (users) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (weaknesses) -> Weakness (critical_reports) -> TeamMemberGroupConnection (edges) -> TeamMemberGroupEdge (node) -> TeamMemberGroup (team_members) -> TeamMember (team) -> Team (audit_log_items) -> AuditLogItem (source_user) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill
- Query (webhook) -> Webhook (created_by) -> User (pentester_profile) -> PentesterProfile (skills) -> Skill

```

```shell
$ graphql-path-enum --help
graphql-path-enum 1.0
dee-see (https://gitlab.com/dee-see/graphql-path-enum)
Use this tool to list the different paths that lead to one object in a GraphQL schema.

USAGE:
    graphql-path-enum [FLAGS] --introspect-query-path <FILE_PATH> --type <TYPE_NAME>

FLAGS:
        --expand-connections    Expand connection nodes (with pageInfo, edges, etc. edges), they are skipped by default
    -h, --help                  Prints help information
    -V, --version               Prints version information

OPTIONS:
    -i, --introspect-query-path <FILE_PATH>    Path to the introspection query result saved as JSON
    -t, --type <TYPE_NAME>                     The type to look for in the graph.
```
